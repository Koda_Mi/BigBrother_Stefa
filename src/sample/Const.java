package sample;

public class Const {
    public static final String USER_TABLE = "users";

    public static final String USERS_ID = "id";
    public static final String USERS_NAME = "name";
    public static final String USERS_LASTNAME = "lastname";
    public static final String USERS_USERNAME = "username";
    public static final String USERS_PASSWORD = "password";
    public static final String USERS_EMAIL = "email";
    public static final String USERS_POST_USER = "post_user";

}
