package sample.Controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import sample.Const;

public class BossController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TextField Work;

    @FXML
    private ComboBox<?> Slave;

    @FXML
    private Button Save;

    @FXML
    private Button Back;

    @FXML
    private TextField Name;

    @FXML
    private TextField LastName;

    @FXML
    private TextField UserName;

    @FXML
    void initialize()
    {
        Back.setOnAction(event ->
        {
            openNewScene("/sample/fxml/sample.fxml");
        });



    }
    public void openNewScene(String window)
    {
        Back.getScene().getWindow().hide();

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource(window));

        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Parent root = loader.getRoot();
        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        stage.showAndWait();
    }
    String insert = "SELECT" + Const.USERS_LASTNAME + "," + Const.USERS_NAME + "," + Const.USERS_USERNAME +  "FROM" + Const.USER_TABLE + "WHERE" + Const.USERS_POST_USER + "= Boss";


}



