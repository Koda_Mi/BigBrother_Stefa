package sample.Controllers;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import sample.DatabaseHandler;
import sample.User;

public class SignUpController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private PasswordField password_key;

    @FXML
    private Button SignUpBut;

    @FXML
    private TextField Name;

    @FXML
    private TextField LastName;

    @FXML
    private TextField Post;

    @FXML
    private TextField LoginKey;

    @FXML
    private TextField Email;

    @FXML
    void initialize() {


        SignUpBut.setOnAction(event -> {

            signUpNewUser();

            } );

    }

    private void signUpNewUser()
    {
        DatabaseHandler dbHandler = new DatabaseHandler();

        String name = Name.getText();
        String lastName = LastName.getText();
        String post = Post.getText();
        String email = Email.getText();
        String userName = LoginKey.getText();
        String password = password_key.getText();

        User user = new User(name, lastName, post, email, userName, password);

        dbHandler.signUpUser(user);


    }
}
