package sample.Controllers;

import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import sample.DatabaseHandler;
import sample.Shake;
import sample.User;


public class Controller {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private PasswordField password_key;

    @FXML
    private TextField login_key;

    @FXML
    private Button SignInBut;

    @FXML
    private Button SignUpbut;

    @FXML
    void initialize() {
        SignInBut.setOnAction(event ->
        {
            String loginText = login_key.getText().trim();
            String loginPassword = password_key.getText().trim();

            if (!loginText.equals("") && !loginPassword.equals(""))

                loginUser(loginText, loginPassword);
            else
                System.out.println("Поля заполняй, да");
        });



        SignUpbut.setOnAction(event ->
        {
            openNewScene("/sample/fxml/SignUp.fxml");
        });
    }

    private void loginUser(String loginText, String loginPassword)
    {
        DatabaseHandler dbHandler = new DatabaseHandler();
        User user = new User();
        user.setUserName(loginText);
        user.setPassword(loginPassword);
        ResultSet result = dbHandler.getUser(user);

        int counter = 0;
        while(true)
                {
                    try {
                        if (!result.next()) break;
                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                    }
                    counter++;
                }

        if(counter >= 1)
        {
            openNewScene("/sample/fxml/boss.fxml");
        }else
            {
                Shake userLoginAnim = new Shake(login_key);
                Shake userPassAnim = new Shake(password_key);
                userLoginAnim.playAnim();
                userPassAnim.playAnim();
            }
    }
    public void openNewScene(String window)
    {
        SignUpbut.getScene().getWindow().hide();

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource(window));

        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Parent root = loader.getRoot();
        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        stage.showAndWait();
    }
}



