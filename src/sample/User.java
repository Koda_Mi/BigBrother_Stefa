package sample;

public class User {
    private String Name;
    private String lastName;
    private String post;
    private String userName;
    private String password;
    private String email;

    public User(String name, String lastName, String post, String userName, String password, String email) {
        Name = name;
        this.lastName = lastName;
        this.post = post;
        this.userName = userName;
        this.password = password;
        this.email = email;
    }

    public User() {

    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
